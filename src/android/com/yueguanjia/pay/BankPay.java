package com.yueguanjia.pay;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.webkit.WebView;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaArgs;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Milo on 16/7/5.
 */
public class BankPay extends CordovaPlugin {

    private CallbackContext callbackContext;

    // 插件返回值
    private JSONObject result;

    /**
     * 插件初始化
     * <param name="onload" value="true" />
     * @param cordova
     * @param webView
     */
    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        this.result = new JSONObject();
    }

    /**
     * 插件初始化
     */
    @Override
    protected void pluginInitialize() {
        super.pluginInitialize();
    }

    /**
     * 验证插件参数是否正确
     * @param action
     * @param args
     * @return
     * @throws JSONException
     */
    public boolean validate(String action, JSONArray args) throws JSONException {
        return action == null || action.equals("") || args == null || args.length() < 1;
    }

    /**
     * 插件入口
     * @param action          The action to execute.
     * @param args            The exec() arguments.
     * @param callbackContext The callback context used when calling back into JavaScript.
     * @return
     * @throws JSONException
     */
    @Override
    public boolean execute(String action, final JSONArray args, final CallbackContext callbackContext) throws JSONException {

        this.callbackContext = callbackContext;

        if (this.validate(action, args)) {

            result.put("success", false);
            result.put("code", "param error");
            result.put("message", "参数action或url不能为空.");

            callbackContext.error("参数action或url不能为空.");

            return false;

        }

        if (action.equals("pay")) {

            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {

                    try {
                        pay(args);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });

        }

        return true;

    }

    /**
     * Activity回调
     * @param requestCode   The request code originally supplied to startActivityForResult(),
     *                      allowing you to identify who this result came from.
     * @param resultCode    The integer result code returned by the child activity through its setResult().
     * @param intent        An Intent, which can return result data to the caller (various data can be
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        if (resultCode == 1001) {

            boolean success = intent.getBooleanExtra("success", false);
            String code = intent.getStringExtra("code");
            String message = intent.getStringExtra("message");

            try {

                result.put("success", success);
                result.put("code", code);
                result.put("message", message);

            } catch (JSONException e) {
                Log.d("ERROR", e.getMessage());
            }

            if (success) {
                callbackContext.success();
            } else {
                callbackContext.error(message);
            }

        }

    }

    /**
     * 支付跳转
     * @param args
     * @throws JSONException
     */
    public void pay(JSONArray args) throws JSONException {

        String url = args.getString(0);

        Intent intent = new Intent();
        intent.setClass(cordova.getActivity(), BankPayActivity.class);
        intent.putExtra("url", url);
        cordova.startActivityForResult(this, intent, 1);

    }

}