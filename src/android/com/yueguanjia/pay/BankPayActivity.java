package com.yueguanjia.pay;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.HttpAuthHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.jielin.customer.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cmb.pb.util.CMBKeyboardFunc;

/**
 * Created by Milo on 16/7/10.
 * 银行支付
 */
public class BankPayActivity extends Activity {

    public final static String STATUS_OK = "ok";

    public final static String STATUS_NO = "no";

    public final static String STATUS_CANCEL = "cancel";

    private WebView webView;

    // 支付状态 (ok/no/cancel)
    private String status = "cancel";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // 自定义标题栏
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.bank_pay_layout);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.bank_pay_title);

        webView = (WebView) findViewById(R.id.bank_pay_web_view);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSaveFormData(false);

        Intent intent = getIntent();
        String url = intent.getStringExtra("url");

        webView.loadUrl(url);

        // 自定义WebViewClient
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                // 创建招商银行安全键盘
                CMBKeyboardFunc kbFunc = new CMBKeyboardFunc(BankPayActivity.this);

                // 判断URL是否为安全键盘访问地址
                if (kbFunc.HandleUrlCall(view, url) == false) {
                    return super.shouldOverrideUrlLoading(view, url);
                } else {
                    return true;
                }

            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {

                // 验证支付状态
                if (url.endsWith("MB_EUserP_PayOK")) {
                    setStatus(BankPayActivity.STATUS_OK);
                } else if (url.endsWith("MB_EUserP_PayError")) {
                    setStatus(BankPayActivity.STATUS_NO);
                } else if (isBackUrl(url)) {

                    Intent intent = new Intent();
                    intent.putExtra("success", true);
                    intent.putExtra("code", "ok");
                    intent.putExtra("message", "支付成功");

                    setResult(1001, intent);
                    finish();

                } else {
                    setStatus(BankPayActivity.STATUS_CANCEL);
                }

            }

        });

        // 未完成支付返回
        ImageView backBtn = (ImageView) findViewById(R.id.header_left_btn);

        backBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if (status.equals(BankPayActivity.STATUS_OK)) {

                    Intent intent = new Intent();
                    intent.putExtra("success", true);
                    intent.putExtra("code", "ok");
                    intent.putExtra("message", "支付成功");

                    setResult(1001, intent);
                    finish();

                } else if (status.equals(BankPayActivity.STATUS_NO)) {

                    AlertDialog dialog = new AlertDialog.Builder(BankPayActivity.this)
                            .setTitle("提示")
                            .setMessage("支付失败,确定退出支付吗?")
                            .setPositiveButton("确定", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    dialog.dismiss();
                                    Intent intent = new Intent();
                                    intent.putExtra("success", false);
                                    intent.putExtra("code", "no");
                                    intent.putExtra("message", "支付失败");
                                    setResult(1001, intent);
                                    finish();

                                }

                            })
                            .setNegativeButton("取消", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }

                            }).show();

                } else {

                    AlertDialog dialog = new AlertDialog.Builder(BankPayActivity.this)
                            .setTitle("提示")
                            .setMessage("支付未完成,您确定退出支付吗?")
                            .setPositiveButton("确定", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    dialog.dismiss();
                                    Intent intent = new Intent();
                                    intent.putExtra("success", false);
                                    intent.putExtra("code", "cancel");
                                    intent.putExtra("message", "支付取消");
                                    setResult(1001, intent);
                                    finish();

                                }

                            })
                            .setNegativeButton("取消", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }

                            }).show();

                }

            }
        });

    }

    /**
     * 是否为悦管家URL
     * @param url
     * @return
     */
    public boolean isBackUrl(String url) {

        Pattern p = Pattern.compile("^(http|www)?(://)?([\\w]+)([\\.]{1})(yueguanjia.com){1}([\\w./#-]{0,})$", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(url);

        return m.matches();

    }

    /**
     * 设置WebView的支付状态
     * @param status
     */
    public void setStatus(String status) {
        this.status = status;
    }

}