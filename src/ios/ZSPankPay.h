//
//  ZSPankPay.h
//  悦管家
//
//  Created by apple on 16/7/4.
//
//

#import <Cordova/CDV.h>
#import <UIKit/UIKit.h>
//导入安全键盘需要的库
#import <cmbkeyboard/CMBWebKeyboard.h>
#import <cmbkeyboard/NSString+Additions.h>


//招商银行支付插件接口
@interface ZSPankPay : CDVPlugin<UIWebViewDelegate,UIGestureRecognizerDelegate,UIAlertViewDelegate>

//回调Id
@property (nonatomic,strong) NSString *CallbackId;
//UrlScheme 应用的scheme
@property (nonatomic,strong) NSString *testUrl;
//当前的URL
@property (nonatomic,strong) NSString *currentURL;
//当前的HTML
@property (nonatomic,strong) NSString *currentHTML;
//判断是否点击返回操作
@property (nonatomic,assign) BOOL isBack;
//网页视图
@property (nonatomic,strong) UIWebView * webviews;
//模拟导航栏
@property (nonatomic,strong) UIView * navView;
//返回图标和标题label
@property (nonatomic,strong) UIButton * backBtn;
@property (nonatomic,strong) UILabel * titleLabel;

//获取当前页面title
@property (nonatomic,strong) NSString * current_page_title;

//判断支付状态的标识  0(成功)  1(失败)  2(取消)
@property (nonatomic,assign) NSInteger  payState;


//安全键盘单例类
@property (nonatomic,strong) CMBWebKeyboard *secKeyboard;

- (void)echo:(CDVInvokedUrlCommand*)command;
- (void)pay:(CDVInvokedUrlCommand*)command;

@end
