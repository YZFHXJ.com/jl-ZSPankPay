/**
 * Created by apple on 16/7/4.
 */
var exec = require('cordova/exec');
window.zspay = function(args, callback, failback) {
    cordova.exec(
        function(msg){callback(msg);}
        ,function(err){failback(err);}
        , "ZSPankPay", "pay", args);
};
